# Medusa LED Module

![Medusa LED Module](https://shop.edwinrobotics.com/4159-thickbox_default/medusa-led-module.jpg "Medusa LED Module")

[Medusa LED Module](https://shop.edwinrobotics.com/new-products/1242-medusa-led-module.html)

This Medusa LED module is designed to fit in any LED as per your requirement. We are providing a bunch of mixed 5mm colored LED's with this module to be used for various application, the onboard potentiometer will allow you to adjust the series resistance, to control the amount of current for the LED and allows you to control the brightness. We had used the 5mm LED footprint for the board but you can use 3mm LED with this board.


Repository Contents
-------------------
* **/Design** - Eagle Design Files (.brd,.sch)
* **/Panel** - Panel file used for production

License Information
-------------------
The hardware is released under [Creative Commons ShareAlike 4.0 International](https://creativecommons.org/licenses/by-sa/4.0/).

Distributed as-is; no warranty is given.